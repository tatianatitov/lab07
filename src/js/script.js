
document.addEventListener("DOMContentLoaded", function(event) {

    //-------------------------Le jeu

    class Jeu{


        //fonction de départ, initialisation variables, fonction principale dans objet

        constructor(_idSvg, _idPointage){

            console.log("Création du jeu");


            //Propriété snap gardée

            this.s = Snap(_idSvg);

            //autres propriétés. this fait référence à Jeu

            this.sortiePointage = document.querySelector(_idPointage);

            this.grandeurCarre = 20;

            this.grandeurGrille = 15;

        }


        //Foncioaanlités Méthodes

        nouvellePartie(){

            this.finPartie();

            this.affichagePointage(1);

            //passage du paramètre Jeu (utilisation dans classe pomme...)

            this.pomme = new Pomme(this);

            this.serpent = new Serpent(this);

        }


        finPartie(){

            if(this.pomme !== undefined){

                this.pomme.supprimePomme();
                this.pomme = undefined;

            }

            if(this.serpent!== undefined){
                this.serpent.supprimeSerpent();
                this.serpent = undefined;
            }

        }


        affichagePointage(_lePointage){

            this.sortiePointage.innerHTML = _lePointage;

        }

    }


    //--------------------------Le serpent

    class Serpent{

        constructor(_leJeu){
            console.log("Création du serpent");

            //grder paramètre défini

            this.leJeu = _leJeu;

            this.currentX = -1;
            this.currentY = 0;

            this.nextMoveX = 1;
            this.nextMoveY = 0;

            this.touche = false;


            this.serpentLongueur = 1;
            this.tablCarreSerpent = [];

            this.vitesse = 250;
            this.timing = setInterval(this.controleSerpent.bind(this), this.vitesse);


            document.addEventListener("keydown", this.verifTouche.bind(this));

        }


        verifTouche(_evt){

            var evt = _evt;

            console.log(evt.keyCode);

            this.deplacement(evt.keyCode);

        }


        deplacement(dirCode){

            switch(dirCode){

                //gauche
                case 37:
                    this.nextMoveX = -1;
                    this.nextMoveY = 0;
                    break;

                    //haut
                case 38:
                    this.nextMoveX = 0;
                    this.nextMoveY = -1;
                    break;

                    //droite

                case 39:
                    this.nextMoveX = 1;
                    this.nextMoveY = 0;
                    break;

                //bas

                case 40:
                    this.nextMoveX = 0;
                    this.nextMoveY = 1;
                    break;

            }

            //console.log(this.nextMoveX, this.nextMoveY);

        }


        controleSerpent(){

            var nextX = this.currentX + this.nextMoveX;
            var nextY = this.currentY + this.nextMoveY;

            this.tablCarreSerpent.forEach(function(element){
                if(nextX===element[1] && nextY ===element[2]){
                    console.log('moiMM');
                    this.leJeu.finPartie();
                    this.touche = true;
                }
            }.bind(this));
            //limite écran

            if(nextY<0 || nextX<0 || nextY>this.leJeu.grandeurGrille-1 || nextX>this.leJeu.grandeurGrille-1){
                this.leJeu.finPartie();
                this.touche = true;
            }

            //dessin à prochaine position et addition

            if(!this.touche){
                if(this.currentX === this.leJeu.pomme.pomme[1] && this.currentY=== this.leJeu.pomme.pomme[2]){
                    this.serpentLongueur++;
                    this.leJeu.affichagePointage(this.serpentLongueur);
                    this.leJeu.pomme.supprimePomme();
                    this.leJeu.pomme.ajoutePomme();
                }

                this.dessineCarre(nextX, nextY);
                this.currentX = nextX;
                this.currentY = nextY;
            }
        }


        dessineCarre(x, y){

            var unCarre = [this.leJeu.s.rect(x * this.leJeu.grandeurCarre, y * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre), x, y];

            this.tablCarreSerpent.push(unCarre);

            if (this.tablCarreSerpent.length > this.serpentLongueur){

                //tableau à 2 dimensions
                this.tablCarreSerpent[0][0].remove();
                this.tablCarreSerpent.shift();
            }

        }


        supprimeSerpent(){
            clearInterval(this.timing);
            while(this.tablCarreSerpent.length>0){
                this.tablCarreSerpent[0][0].remove();
                this.tablCarreSerpent.shift();
            }
        }

    }



    //-----------------------------La pomme

    class Pomme{

        constructor(_leJeu){
            console.log("Création de la pomme");
            //garder en mémoire paramètre. référence à la classe Jeu

            this.leJeu = _leJeu;

            this.pomme = [];

            this.ajoutePomme();

        }


        ajoutePomme(){

            //création grille. création svg

            var posX = Math.floor(Math.random() * this.leJeu.grandeurGrille);
            var posY = Math.floor(Math.random() * this.leJeu.grandeurGrille);

            //référence à pomme pour pouvoir l'enlever

            this.pomme = [this.leJeu.s.rect(posX * this.leJeu.grandeurCarre, posY * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre).attr({fill:'red'}), posX, posY];


        }


        supprimePomme(){

            this.pomme[0].remove();

        }

    }


    var unePartie = new Jeu("#jeu", "#pointage");


    //départ au click du bouton

    var btnJouer = document.querySelector("#btnJouer");
    btnJouer.addEventListener('click', nouvellePartie);

    function nouvellePartie(){
        unePartie.nouvellePartie();
    }


});